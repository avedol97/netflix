package modelDto;

import java.util.List;

public class ListDto {
    List<MovieDto> movies;

    public ListDto(List<MovieDto> movies) {
        this.movies = movies;
    }
    public List<MovieDto> getMovies() {
        return movies;
    }
}
