package com.example.demo.repository;


import modelDto.MovieDto;

import java.util.List;

public interface MovieRepository {

    List<MovieDto> getMovie();
}
