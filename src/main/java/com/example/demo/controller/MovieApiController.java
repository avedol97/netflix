package com.example.demo.controller;

import com.example.demo.service.MovieService;
import modelDto.ListDto;
import modelDto.MovieDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.CrossOrigin;
import org.springframework.web.bind.annotation.GetMapping;
import org.springframework.web.bind.annotation.RestController;

import java.util.List;
import java.util.Map;

@CrossOrigin
@RestController
public class MovieApiController {


    @Autowired
    MovieService movieService;


    @GetMapping("/movies")
    public ResponseEntity<ListDto> getMovies(){


        return new ResponseEntity<>(new ListDto(movieService.getMovie()), HttpStatus.OK);
    }


}
