package com.example.demo.service;

import modelDto.MovieDto;

import java.util.List;


public interface MovieService {
    List<MovieDto> getMovie();
}
