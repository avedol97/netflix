package com.example.demo.service;

import com.example.demo.repository.MovieRepository;
import com.example.demo.repository.MovieRepositoryImpl;
import modelDto.MovieDto;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.util.List;

@Service
public class MovieServiceImpl implements MovieService{

    @Autowired
    MovieRepository movieRepository;

    public List<MovieDto> getMovie(){
        return movieRepository.getMovie();
    }
}
